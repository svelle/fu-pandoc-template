---
lecturer: Name der Dozentin
tutor: Name der Tutorin
tutorialnum: Nummer des Tutoriums
exercisenum: 00
coursename: Name der Lehrveranstaltung
semyear: Semester Jahr
author: Namen der StudentInnen
---
[//]: # (examplecomment: this will not appear in the document!)


# Aufgabe: Quellcode einbinden \hfill (5 Punkte)
*Aufgabenstellung des Dozenten*

Haskell Code mit Syntax-Highlighting im Dokument (Highlighting mit Markdown ist für alle Sprachen verfügbar, die im LaTex-"listings"-package [hier](http://texdoc.net/texmf-dist/doc/latex/listings/listings.pdf#page=13) aufgelistet sind):

```Haskell
    qsort :: Ord a => [a] -> [a]
    qsort []     = []
    qsort (x:xs) = qsort kleinergl ++ [x] ++ qsort groesser
                   where
                      kleinergl = [y | y <- xs, y <= x]
                      groesser  = [y | y <- xs, y > x]
```

Python Code mit Syntax-Highlighting von einem eingebundenen Skript:

\lstinputlisting[style=py, caption={Beispiel: Einbindung von Programmcode}, firstnumber={1}, firstline={1}, lastline={7}]
{./examplecode.py}

Die zum Syntax-Highlighting verfügbaren Programmiersprachen sind in der Datei `templates/header.tex` definiert und können dort auch noch erweitert werden.
Derzeit verfügbar mit Kürzel:

 * **Python**: py
 * **Haskell**: hs
 * **LaTex**: tex
 * **Java**: java
 * **C**: c

\newpage

Falls man auf die gleiche Art und Weise aus irgendeinem Grund mit LaTex anstatt mit Markdown (wie oben gezeigt) Code im Dokument highlighten will, dann macht man das folgendermaßen:

\begin{lstlisting}[style=hs, caption={Beispiel: Einbindung von Haskell-Programmcode}]
data PhiNum a = PhiNum { numPart :: a, phiPart :: a } deriving (Eq, Show)

instance Num a => Num (PhiNum a) where
    fromInteger n = PhiNum (fromInteger n) 0
    PhiNum a b + PhiNum c d = PhiNum (a+c) (b+d)
    PhiNum a b * PhiNum c d = PhiNum (a*c+b*d) (a*d+b*c+b*d)
    negate (PhiNum a b) = PhiNum (-a) (-b)
    abs = undefined
    signum = undefined

fib n = phiPart $ PhiNum 0 1 ^ n
\end{lstlisting}


# Aufgabe: Mathemodus \hfill (5 Punkte)
a) *Welche nützlichen Befehle gibt es?*

 * Mengen: $\N, \Z, \Q, \R$
 * Widerspruch: $\Bolt$
 * $\zz$, \gdw, \oBdA
 * runden: $\floor{\frac{n}{2}}, \ceil{\frac{n}{3}}$
 * Funktion: $f \from \N \to \N$
 * Komplexitätsklassen: $\P, \NP$
 * $\Landau$-Notation
 * Wahrheitswerte: $\True, \False$
 * Währung: \Eu

b) *Fallunterscheidung*
\begin{equation}
	\text{collatz}(x) =
		\begin{cases}
			\floor{\frac{n}{2}}, & \text{falls } n \mod 2 = 0 \\
			3\cdot n+1, & \text{sonst} \\
		\end{cases}
\end{equation}
	
c) *Matrix*
\begin{equation}
	A = \begin{pmatrix}
		1 & 2 & 3 \\
		4 & 5 & 6 \\
	\end{pmatrix}
\end{equation}


# Aufgabe: Zeichnen mit tikz \hfill (10 Punkte)
*Zeichnen Sie einen Automaten*

\begin{figure}[H]
\centering
\begin{tikzpicture}[>=stealth',
	shorten >=1pt,
    auto,
	node distance=5em,
    scale = 1,
    transform shape]
	% Quelle: http://tex.stackexchange.com/questions/45734/drawing-graphs-in-latex
	% erster Strang
    \node[initial,state]   (A)              {$q_0$};
    \node[state]           (B) [above right of=A] {$q_1$};
    \node[state,accepting] (C) [below right of=A] {$q_2$};

	\path[->] % zeichnet die Pfeile von (A) nach (C)
      (A) edge                 node         {$1$} (B)
      (A) edge                 node [left] {$0$} (C)
      (B) edge [loop right]    node         {$0$} (B)
      (B) edge                 node         {$1$} (C)
      (C) edge [bend right=45] node [right] {$1$} (B)
      (C) edge [loop right]    node         {$0$} (C);
	% (startknoten)
		% Option: "edge [bend right=45]" macht Kurve um 45° nach rechts
		% Option "edge [loop]" macht rekursiven Pfeil
		% "node [above, below, right, left, sloped, align=center, pos=0.5] {text}" für Beschriftung
		% (zielknoten)
		% sloped dreht text zur pfeilrichtung
		% right, left richtet text rechts bzw. links vom Pfeil aus (nicht ganz eindeutig)
		% pos aus [0,1] verschiebt text entlang der Pfeilrichtung, 0.5 ist mittig
		% above, below richtet text ober- bzw. unterhalb des Pfeils aus
		% align legt Ausrichtung des texts fest
\end{tikzpicture}
\end{figure}


# Aufgabe: Tabelle
*Zeichnen Sie eine Tabelle*

Test1 | Test2 | Test3
:-----|:-----:|------:
Alpha | Beta  | Gamma
 Eins | Zwei  | Drei
